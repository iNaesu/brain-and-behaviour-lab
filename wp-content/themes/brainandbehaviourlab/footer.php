    <footer id="foot">

        <div id="legal">

            <p>
                &copy; 2002-15 The University of Sydney.
                <strong>Last&nbsp;updated:</strong>&nbsp;
                <?php the_modified_time('F jS, Y') ?>
            </p>

            <p>
                <strong>ABN:</strong>&nbsp;15&nbsp;211&nbsp;513&nbsp;464.
                <strong>CRICOS number:</strong>&nbsp;00026A.
                <strong>Phone:</strong>&nbsp;+61&nbsp;2&nbsp;9351&nbsp;2222.
            </p>

            <p>
                <strong>Authorised&nbsp;by:</strong>&nbsp;Head of School, School of Psychology.
            </p>

            <p id="foot-links">
                <a href="http://sydney.edu.au/contact-us.html">Contact the University</a> |
                <a href="http://sydney.edu.au/disclaimer.html">Disclaimer</a> |
                <a href="http://sydney.edu.au/privacy-policy.html">Privacy</a> |
                <a href="http://sydney.edu.au/accessibility.html">Accessibility</a>
                <br/>
            </p>

         </div><!-- #legal -->
    </footer><!-- #foot -->

	<?php wp_footer(); ?>
</body>
</html>
