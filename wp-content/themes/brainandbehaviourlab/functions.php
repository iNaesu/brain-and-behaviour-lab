<?
/**
 * Site level Widgets - Register widget area 
 *
 * See: http://devotepress.com/wordpress-coding/how-to-register-sidebars-in-wordpress/
 */
 
function shape_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Primary Widget Area', 'shape' ),
        'id' => 'sidebar-1',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<dt>',
        'after_title' => '</dt>',
    ) );
 
    register_sidebar( array(
        'name' => __( 'Secondary Widget Area', 'shape' ),
        'id' => 'sidebar-2',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<dt>',
        'after_title' => '</dt>',
    ) );
}
add_action( 'widgets_init', 'shape_widgets_init' );





/**
 * Page level Widgets - Register our sidebars and widgetized areas.
 * See: http://codex.wordpress.org/Widgetizing_Themes
 */
function arphabet_widgets_init() {

	register_sidebar( array(
		'name'          => 'Right sidebar',
		'id'            => 'right_sidebar_1',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'arphabet_widgets_init' );



/*
##############################################################
## Header image - we can use this in footer / elsewhere though
/*
$defaults = array(
	'default-image'          => '',
//	'default-image'          => get_template_directory_uri() . '/images/header.jpg', 
	'width'                  => 957,
	'height'                 => 0, # must be zero !!! No, really.
	'flex-height'            => true,
	'flex-width'             => false,
	'uploads'                => true,
	'random-default'         => true,
	'header-text'            => true,
	'default-text-color'     => '',
	'wp-head-callback'       => '',
	'admin-head-callback'    => '',
	'admin-preview-callback' => ''
);
*/

$defaults = array(
	'flex-height'            => true,
	'flex-width'             => false,
	'width'                  => 957, 
	'height'                 => 15  # Can't upload header images if zero *cry*
);
add_theme_support( 'custom-header', $defaults );


?>

