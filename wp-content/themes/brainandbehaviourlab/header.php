<?
/*
-- Notes --

*/
#### GLOBALS ####
$page_id = $_GET["page_id"]; // or: the_ID();
$activeTabName = ""; # value determined below
$labTitle = wp_title( '', false, 'right' ); # also use bloginfo( 'name' );
$topNavDepth = 1; # USyd template only designed to show one level of depth in top-menu

###########################################################
#### Top Tabs
###########################################################
# FROM wp-includes\nav-menu-template.php :: wp_list_pages()
#   >> post-template.php :: wp_page_menu():898 -> 929 -> wp_list_pages() -> walk_page_tree()
$args = '';
$defaults = array(
	'depth' => 0, 'show_date' => '',
	'date_format' => get_option('date_format'),
	'child_of' => 0, 'exclude' => '',
	'title_li' => '', 'echo' => 0,
	'authors' => '',
    'sort_column' => 'menu_order, post_title',
	'link_before' => '', 'link_after' => '', 'walker' => '',
);

$r = wp_parse_args( $args, $defaults );
extract( $r, EXTR_SKIP );

$output = '';
$current_page = 0;

// sanitize, mostly to keep spaces out
$r['exclude'] = preg_replace('/[^0-9,]/', '', $r['exclude']);

// Allow plugins to filter an array of excluded pages (but don't put a nullstring into the array)
$exclude_array = ( $r['exclude'] ) ? explode(',', $r['exclude']) : array();
$r['exclude'] = implode( ',', apply_filters('wp_list_pages_excludes', $exclude_array) );

$r['title_li'] = ''; # rm Pages node
# Query pages.
$r['hierarchical'] = 0;
$pages = get_pages($r);
//echo "\n Test:". $pages[0]->post_title ."\n<p>\n"; //=>[0]=>post_title
//var_dump($pages);
$tabHtml = "";
foreach ( (array) $pages as $key => $p )
{
	$id = $p->ID;
	if($p->post_parent == 0)
	{
		$menutitle = $p->post_title;

		# highlight active tab
		# if current $page_id == $id then highlight as active
		# problem: sub items for current tab are hidden when highlighted
		$strActiveTab = ($id == $page_id) ? "class='active'" : ""; # hidden for current page in css

		if($id == $page_id) $activeTabName = $menutitle;
		//$menutitle ." <br>\n"; //=>[0]=>post_title

		## child pages
		$childHtml = "";
		$r = array('child_of' => $id, 'exclude' => '', 'title_li' => '');
		$pages = get_pages($r);
		$tempChildHtml = walk_page_tree($pages, $topNavDepth, $current_page, $r);
		if(strlen($tempChildHtml)>1) $childHtml = "\n\t\t<ul>$tempChildHtml</ul>";
		//echo "\n\n child pages:\n\n";



		# tab html
		$tabHtml .= "
		<li $strActiveTab>
			<span><a href='./?page_id=$id'><span>$menutitle</span></a></span> $childHtml
		</li>
			";
	}
}

/* -- Example Tab HTML (USyd template) --
	<li>
		<span><a href='/gambling_treatment_clinic/about_us/index.shtml'><span>About Us</span></a></span>
		<ul><li><a href='/gambling_treatment_clinic/about_us/staff.shtml'>Staff</a></li><li><a href='/gambling_treatment_clinic/about_us/mission_statement.shtml'>Mission Statement</a></li><li><a href='/gambling_treatment_clinic/about_us/internship.shtml'>Internship</a></li><li><a href='/gambling_treatment_clinic/about_us/news_2014.shtml'>News</a></li></ul>
	</li>

	<li class="active">
		<span><a href='/gambling_treatment_clinic/events/index.shtml'><span>Events</span></a></span>
		<ul><li><a href='/gambling_treatment_clinic/events/index.shtml'>Events Calendar</a></li><li><a href='/gambling_treatment_clinic/events/video.shtml'>Events Video</a></li></ul>
	</li>
*/
###########################################################
#### Lefts sidebar
###########################################################
# See: https://wordpress.org/support/topic/how-to-know-the-id-of-current-page-and-top-parent-page
## Find the top-most parent for this page
global $tabHasChildren;
$tmp_post = $post;
$tmp_parent_id = $post->post_parent;
while ($tmp_parent_id) {
    $tmp_post = &get_post($tmp_parent_id);
    $tmp_parent_id = $tmp_post->post_parent;
}
//var_dump ($tmp_post);
//if ($tmp_parent_id != $page_id) {
$topParenPost = $tmp_post;
$topParenTitle = $topParenPost->post_title;
//}

## fetch child pages
$childDepth = 0;
$childHtml = "";
# use $r from Top menu (previous code section)
//$r = array('child_of' => $topParenPost->ID, 'exclude' => '', 'title_li' => '');
$r['child_of'] = $topParenPost->ID;
$r['depth'] = $childDepth;

/*	$defaults = array( 'menu' => '', 'container' => 'div', 'container_class' => '', 'container_id' => '', 'menu_class' => 'menu', 'menu_id' => '',
	'echo' => true, 'fallback_cb' => 'wp_page_menu', 'before' => '', 'after' => '', 'link_before' => '', 'link_after' => '', 'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
	'depth' => 0, 'walker' => '', 'theme_location' => '' );
/* */


$pages = get_pages($r);
$tempChildHtml = walk_page_tree($pages, $childDepth, $current_page, $r);
//if(strlen($tempChildHtml)>1) $childHtml = "\n\t\t<ul>$tempChildHtml</ul>";
if( strlen($tempChildHtml) > 0) $tabHasChildren=true;
# if an element has children in USyd template it must have class 'static', otherwise child pages are invisible!
$tempChildHtml = str_replace("page_item_has_children","static", $tempChildHtml);


if($tabHasChildren)
{
  $leftSideBar = "
	<!-- *** Left sidebar ***  -->
	<!-- (menu div) -->
		<div id='menu' class='withtabs'>
			<dl>
				<dt>$topParenTitle</dt>
				<dd>
					<ul>
					  $tempChildHtml
					</ul>
				</dd>
			</dl>
		</div>
	<!-- (END menu div) -->
  ";
}

###########################################################
###########################################################
/* -- Example Head (Wordpress template) --
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" />
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
<![endif]-->
<?php wp_head(); ?>
*/

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="eng" lang="eng">
<head>
	<title><? bloginfo( 'name' ); ?> - The University of Sydney</title>
	<!--
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
	-->

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="Content-Script-Type" content="text/javascript" />
	<!-- CMS Version: 3.0  -->
	<meta name="DC.Description" content="<? bloginfo( 'name' ); ?>." />
	<meta name="DC.Webmaster" content="websupport_at_usyd.edu.au" />
	<meta name="DC.Created" content="24/06/2014" />
	<meta name="DC.Subject" content="<? bloginfo( 'name' ); ?>" />
	<meta name="DC.Modified" content="05/08/2014" />
	<meta name="DC.Publisher" content="Publications Office" />
	<meta name="DC.Valid" content="05/04/2015" />
	<meta name="DC.Creator" content="Publications Office" />
	<meta name="DC.Title" content="<? bloginfo( 'name' ); ?>" />


	<!--	<link rel="stylesheet" href="http://www.psych.usyd.edu.au/styles/base_internal.css" type="text/css" /> -->
	<link rel="stylesheet" media="screen" href="http://www.psych.usyd.edu.au/styles/screen.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" media="screen" href="http://www.psych.usyd.edu.au/styles/screen-ie.css" type="text/css" /><![endif]-->
	<!--[if IE 6]><link rel="stylesheet" media="screen" href="http://www.psych.usyd.edu.au/styles/screen-ie6.css" type="text/css" /><![endif]-->
	<link rel="stylesheet" media="screen" href="http://www.psych.usyd.edu.au/styles/screen-local.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" media="screen" href="http://www.psych.usyd.edu.au/styles/screen-local-ie.css" type="text/css" /><![endif]-->
	<!--[if IE 6]><link rel="stylesheet" media="screen" href="http://www.psych.usyd.edu.au/styles/screen-local-ie6.css" type="text/css" /><![endif]-->
	<link rel="stylesheet" media="print" type="text/css" href="http://www.psych.usyd.edu.au/styles/print.css" />

    <!-- 2015 rebranding update -->
    <link rel="stylesheet" media="screen" href="http://sydney.edu.au/uswt/v5/compatibility/styles/local-phoenix.css"
    type="text/css" />
    <!-- /2015 -->

	<!-- WordPress Customisations -->
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/style.css"/>

<!-- Google Analytics: --
<script type="text/javascript" src="/scripts/googleanalytics.js"></script>
-->

	<meta type="Page Format Style" content="DEFAULT" />
<?/*
	<? wp_head(); ?>
*/?>
</head>
<body class="" onload="">
<div id="w1">
	<div id="w2">
		<div id="w3">
			<div id="head">
		<a class='skip-nav' href='#content'>Skip to main content</a>
	<div id="masthead">
	<h1>
		<a id="logo" href="http://sydney.edu.au">The University of Sydney</a>
		<span id="separator">-</span>
		<span id="tag-line"><? bloginfo( 'name' ); ?></span>
	</h1>
</div>
	<!-- (global nav) -->
        <!-- 2015 Branding - this loads the new top nav bar -->
        <script type="text/javascript" src="http://sydney.edu.au/uswt/v5/compatibility/scripts/local-phoenix.js"></script>
        <!-- /2015 -->

		<!-- start global nav -->
		<form id="search" action="http://search.usyd.edu.au/search/search.cgi">
			<input type="hidden" name="collection" value="Usyd" />
			<input type="text" name="query" class="field" title="Enter search terms" value="Enter search terms"/>
			<input type="submit" value="Go" class="button" />
<!--			<input type="hidden" name="scope" value="www.usyd.edu.au/unit" />   -->
        </form>
        <ul id="nav-global">
          <!-- use the "active" class to define the currently active item (highlighted text and nav indicator) -->
          <li class="active"><a href="./"><? bloginfo( 'name' ); ?></a></li>
          <li><a href="http://sydney.edu.au/science/psychology/">School of Psychology</a></li>
          <li><a href="/about_us/contact_us.shtml">Contact Us</a></li>
          <li><a href="http://sydney.edu.au/">University Home</a></li>
        </ul>
        <!-- end global nav -->
	<!-- (global nav) -->
</div>
<!-- breadcrumb -->
<div class="breadcrumb"><span class="prefix">You are here: </span>
		<a href="http://www.psych.usyd.edu.au/">Home</a>
		/ <a href="./"><? bloginfo( 'name' ); ?></a>
		/ <?=$activeTabName ?> <!-- Current Page -->
</div>
<!-- (tabs pane) -->

<!-- templateVersion="3.0" -->
<!-- beginnoindex -->
<div id="tabbar">
        <ul id="tabs" class="horizontal">
<!--TAB_BEGIN-->
<?= $tabHtml ?>
<!--TAB_END-->
   </ul>
</div>
<!-- endnoindex -->

<div id="tabunderscore"></div>
<!-- ( end tabs pane) -->


<!--   ##############   HEADER IMAGE    #######################  -->
<center>
<img style="margin: 10px 0px 0px 0px; border: 0px #FFF;" src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="" />
</center>
<!-- /header image -->

<div id="mid" class="clearfix">



<!--   ##############   Sidebar    #######################  -->
<?php
##### Right Sidebar ############################
get_sidebar();
?>


<?
echo $leftSideBar;
/*
-- USyd Menu :: Left Sidebar: --
	<!-- (menus pane) -->
		<div id='menu' class='withtabs'>
			<dl>
				<dt>Events</dt>
				<dd>
					<ul>
					  <li><a class="active" href='/news'>News</a></li>
					  <li><a href='/research'>Research Areas</a></li>
					</ul>
				</dd>
			</dl>
		</div>
	<!-- (END menus pane) -->


-- GTC --


<div id="menu" class="withtabs">
<dl>
<dt>Research</dt>
<dd>
 <ul>

    <li><a href="/science/psychology/gambling_treatment_clinic/research/current_research.shtml">Current Research</a></li>

    <li class="static"><a class="active" href="/science/psychology/gambling_treatment_clinic/research/published_research_blaszczynski.shtml">Published Research</a>
    <ul>
        <li><a href="/science/psychology/gambling_treatment_clinic/research/published_research_blaszczynski.shtml">Alex Blaszczynski</a></li>

        <li><a href="/science/psychology/gambling_treatment_clinic/research/published_research_walker.shtml">Michael Walker</a></li>
    </ul>
    </li>
    <li><a href="/science/psychology/gambling_treatment_clinic/research/gambling_journals.shtml">Gambling Journals</a></li>
 </ul>
</dd>
</dl>
</div>



*/
?>

