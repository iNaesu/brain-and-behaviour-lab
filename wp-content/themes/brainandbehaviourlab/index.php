<?php get_header(); ?>

<?
/*
-- Page width modifiers --

nomenu:   hide left sidebar
nofeature: hide right sidebar

Default, with left + right sidebar: 
	<div id="content"> 
Normal, no right sidebar: 
	<div id="content" class="withtabs nofeature">
Wide: 
	<div id="content" class="withtabs nomenu nofeature">

*/

## this code must be after get_header()
//global $tabHasChildren;
if($tabHasChildren) $strmenu = "";   # $tabHasChildren is defined in header.php
else  $strmenu = " nomenu ";
//var_dump($tabHasChildren);
?>




<!--   ##############   Main Content    #######################  -->
<div id="content" class="withtabs <?= $strmenu ?>"> 
 <div id="w4">

  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<p>
	<h1><?php the_title(); ?></h1>

	<p><?php the_content(__('(more...)')); ?></p>
	<p>&nbsp;</p>

  <?php endwhile; else: ?>

  <p><?php _e('Sorry, no posts matched your criteria.'); ?></p><?php endif; ?>
	<!-- </div> --><!-- end w4 -->

	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>


 </div> <!-- /w4 -->
</div><!-- content end -->


</div><!-- end mid -->

<?php get_footer(); ?>
