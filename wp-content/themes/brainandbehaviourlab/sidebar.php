<?php
/*
<!--   ##############   Sidebar    #######################  -->

--- Doco ----

Good example of adding a widget. Appears to be buggy? Widget overrode main content on my page!
http://themeshaper.com/2012/11/08/the-wordpress-theme-sidebar-and-footer-templates/



--- USyd Template: ----

<div id="sidebar" class="withtabs"> 
	<dl class="feature clearfix "> 
		<dt>Contact</dt>
		<dd class="content">
			<ul>
				<li><a href="/science/psychology/about_us/contact_us.shtml">Contact Us</a></li>
			</ul>
		</dd>
	</dl>
</div> 



******************************************************** 
* Used this example as a guide: 
*  http://themeshaper.com/2012/11/08/the-wordpress-theme-sidebar-and-footer-templates/
* The Sidebar containing the main widget areas.
*
* @package Shape
* @since Shape 1.0
*/
?>
<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
 <div id="sidebar" class="withtabs"> 
   <!-- <dl class="feature clearfix "> -->
    <?php do_action( 'before_sidebar' ); ?>
    <?php dynamic_sidebar( 'sidebar-1' ); ?>
    
    <?php if ( is_active_sidebar( 'sidebar-2' ) ) : ?>
      <hr>
      <?php dynamic_sidebar( 'sidebar-2' ); ?>
 	<?php endif; ?>

   <!-- </dl> -->
 </div> 
<!-- #secondary .widget-area -->
<?php endif; ?>

